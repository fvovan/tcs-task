name := "tcs-task"

version := "1.0"

scalaVersion := "2.11.4"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq( "com.typesafe.akka" %% "akka-actor" % "2.3.8",
                             "com.typesafe" % "config" % "1.2.1",
                             "org.scalatest" %% "scalatest" % "2.2.1" % "test",
                             "com.typesafe.akka" %% "akka-testkit" % "2.3.8" % "test" )

