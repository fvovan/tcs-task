package ru.vfinko

import akka.actor.SupervisorStrategy._
import akka.actor._
import akka.event.LoggingReceive
import scala.concurrent.duration.FiniteDuration

object Arbiter {
  case object StartMatch
  case object SetFinished
  case object MatchFinished

  case class SavePings(count: Int)
  case object Ready
  case object RestoreReceiver

  def props( pingInterval: FiniteDuration, errorInterval: FiniteDuration, pingsToPong: Int, pongsToChange: Int, changesToFin: Int ): Props =
    Props(new Arbiter(pingInterval, errorInterval, pingsToPong, pongsToChange, changesToFin))
}

class Arbiter(pingInterval: FiniteDuration, errorInterval: FiniteDuration, pingsToPong: Int, pongsToChange: Int, changesToFin: Int ) extends Actor with ActorLogging  {
  import Arbiter._
  import Player._

  var currentSet = 1
  var pingsState = 0 //saved

  var homePlayer = context.actorOf( Player.props, "HomePlayer" )
  var awayPlayer = context.actorOf( Player.props, "AwayPlayer" )

  var readyStatus = Map[ActorRef, Boolean](homePlayer -> false, awayPlayer -> false)
  var roles = (homePlayer, awayPlayer) //server : receiver

  override val supervisorStrategy =
    OneForOneStrategy() {
      case _: ReceiverException =>  Restart
    }

  def receive: Receive = LoggingReceive {
    case StartMatch => {
      log.debug("Start Match")
      pingsState = 0
      initPlayers()
    }

    case Ready => {
      readyStatus = readyStatus.updated(sender(), true )
      serve()
    }

    case SetFinished => {
      log.debug("Set # {} finished " , currentSet )
      if (currentSet >= changesToFin ) {
        //context.stop(self)
        log.debug( "Match Ended" )
        context.system.shutdown()
      }
      else {
        currentSet += 1
        changeRoles()
      }
    }
    case SavePings(pings) => pingsState = pings

    case RestoreReceiver => {
      roles._1 ! CancelServe
      initReceiver()
    }
  }

  private[this] def serve(): Unit = {
    if (readyStatus.values.forall(_ == true)) {
      roles._1 ! Serve(pingInterval, roles._2)
    }
  }

  private[this] def changeRoles(): Unit = {
    roles = (roles._2 , roles._1)
    initPlayers()
  }

  private[this] def initPlayers(): Unit = {
    initServer()
    initReceiver()
  }

  private[this] def initServer() : Unit = {
    val (server, _) = roles
    readyStatus = readyStatus.updated(server, false)
    server ! BeServer(pongsToChange)
  }

  private[this] def initReceiver() : Unit = {
    val (_, receiver) = roles
    readyStatus = readyStatus.updated(receiver, false)
    receiver ! BeReceiver( pingsState, pingsToPong, errorInterval, flaky = true )
  }
}
