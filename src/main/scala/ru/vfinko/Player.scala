package ru.vfinko

import akka.actor._
import scala.concurrent.duration._

object Player {
  case object Ping
  case object Pong

  case class Serve( interval: FiniteDuration , receiver: ActorRef )
  case object CancelServe

  case class BeServer(pongsToChange: Int)
  case class BeReceiver(pingsState: Int, pingsToPong: Int, errorInterval: FiniteDuration, flaky: Boolean = false )

  case object KillSelf
  class ReceiverException extends Exception("Receiver kaput")

  def props: Props = Props(new Player)
}

class Player extends Actor with ActorLogging  {
  import Player._
  import Arbiter._
  import scala.concurrent.ExecutionContext.Implicits.global
  import akka.event.LoggingReceive

  var scheduledTasks = List[Cancellable]()

  def server( pongs: Int, pongsToChange: Int ): Receive = LoggingReceive {
    changeBehaviour orElse {
      case Serve(interval, receiver) => {
        scheduledTasks = context.system.scheduler.schedule(0 millis, interval) {
          receiver ! Ping
        } :: scheduledTasks
      }

      case CancelServe => cancelTasks()

      case Pong => {
        val incPongs = pongs + 1

        if (incPongs >= pongsToChange) {
          cancelTasks()
          context.parent ! SetFinished
        }
        else {
          context.become(server(incPongs, pongsToChange))
        }
      }
    }
  }

  def receiver(pings: Int, pingsToPong: Int) : Receive = LoggingReceive {
    changeBehaviour orElse {
      case Ping => {
        val incPings = pings + 1
        if (incPings >= pingsToPong) {
          sender ! Pong
          context.become(receiver(0, pingsToPong))
          context.parent ! SavePings(0)
        }
        else {
          context.become(receiver(incPings, pingsToPong))
          context.parent ! SavePings(incPings)
        }
      }

      case KillSelf => throw  new ReceiverException
    }
  }

  def receive: Receive = LoggingReceive { changeBehaviour }

  override def preRestart(cause: Throwable, msg: Option[Any]): Unit = {
    cancelTasks()
    context.parent ! RestoreReceiver
  }

  val changeBehaviour : Receive = {
    case BeServer( pongsToChange ) => becomeServer(pongsToChange)
    case BeReceiver(pingsState, pingsToPong, errorInterval, flaky ) =>
      becomeReceiver(pingsState, pingsToPong, errorInterval, flaky)
  }

  private[this] def becomeServer(pongsToChange: Int) : Unit = {
    cancelTasks()
    context.become( server(0, pongsToChange ) )
    sender ! Ready
  }

  private[this] def becomeReceiver(pingsState: Int, pingsToPong: Int, errorInterval: FiniteDuration, flaky: Boolean): Unit = {
    cancelTasks()

    if (flaky) {
      scheduledTasks = context.system.scheduler.schedule(errorInterval, errorInterval) {
        self ! KillSelf
      } :: scheduledTasks
    }
    context.become(receiver(pingsState, pingsToPong))
    sender ! Ready
  }

  private[this] def cancelTasks() : Unit = {
    scheduledTasks.foreach(_.cancel()) //stop error simulating or serve scheduling
    scheduledTasks = List()
  }
}
