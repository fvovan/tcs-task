package ru.vfinko

import akka.actor.ActorSystem

object TennisMatch extends App {
  import scala.concurrent.duration._
  import com.typesafe.config._
  import Arbiter._

  val system =  ActorSystem("Tennis")

  val conf = ConfigFactory.load()
  val n = Duration( conf.getDuration("tennis-app.n", MILLISECONDS), MILLISECONDS)
  val b = Duration( conf.getDuration("tennis-app.b", MILLISECONDS), MILLISECONDS)
  val m = conf.getInt("tennis-app.m")
  val k = conf.getInt("tennis-app.k")
  val g = conf.getInt("tennis-app.g")
  assert(n < b, "Error Interval Should be more than Pings Interval")

  val arbiter = system.actorOf(Arbiter.props(n, b, m, k, g), "Arbiter")

  arbiter ! StartMatch

}
